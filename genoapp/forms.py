from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm, ModelChoiceField
from genoapp.models import HealthProfile, Person, Relation, GENDERCHOICE, RELATIONSHIP_TYPE


class RegistrationForm(ModelForm):
    username = forms.CharField(label=(u'User Name'), widget=forms.TextInput(attrs={'class':'input-large'}))
    email = forms.EmailField(label=(u'Email Address'))
    birthday = forms.DateField(label=(u'Birthday'), widget=forms.DateInput())
    firstname = forms.CharField(label=(u'First Name'))
    lastname = forms.CharField(label=(u'Last Name'))
    password = forms.CharField(label=(u'Password'), widget=forms.PasswordInput(render_value=False))
    password1 = forms.CharField(label=(u'Verify Password'), widget=forms.PasswordInput(render_value=False))
    gender = forms.ChoiceField(widget=forms.RadioSelect, choices=GENDERCHOICE)

    class Meta:
        model = User
        fields = ['username','password', 'password1','firstname', 'lastname', 'email', 'gender','birthday']

    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError("This username is already taken. Please choose another.")

    def clean(self):
        if self.cleaned_data['password'] != self.cleaned_data['password1']:
            raise forms.ValidationError("The passwords do not match. Please try again.")
        return self.cleaned_data

class LoginForm(ModelForm):
    username = forms.CharField(label=(u'User Name'))
    password = forms.CharField(label=(u'Password'), widget=forms.PasswordInput(render_value=False))


class PersonForm(ModelForm):
    firstname = forms.CharField(label=(u'First Name'))
    lastname = forms.CharField(label=(u'Last Name'))
    #gender = forms.ChoiceField(widget=forms.RadioSelect, choices=GENDERCHOICE)
    relationship_type = forms.ChoiceField(label=(u'Relationship'), widget=forms.Select, choices=RELATIONSHIP_TYPE)
    #related_to = forms.ModelChoiceField(queryset=Relations.objects.all())
    class Meta:
        model = Person
        fields = ['firstname', 'lastname','relationship_type']
