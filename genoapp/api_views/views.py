from django.contrib.auth.models import User
from genoapp.models import Person
from rest_framework import viewsets
from genoapp.serializers import UserSerializer, PersonSerializer
from rest_framework import generics


class UserViewSet(viewsets.ModelViewSet):
    model = User


class PersonViewSet(viewsets.ModelViewSet):
	model = Person