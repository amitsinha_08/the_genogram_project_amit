from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, logout
from django.contrib.auth import login as login_user
from django.template import RequestContext, loader
from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import FormView


#sotiris test
from genoapp.serializeData import *

import pdb

from genoapp.forms import RegistrationForm, LoginForm, PersonForm
from genoapp.models import Person, Relation, HealthProfile, Account
from genoapp import utilities

def index(request):

	#data = {'shapes':'male'}
	return render_to_response('genoapp/Homepage.html', RequestContext(request))
	#template = loader.get_template('genoapp/index.html')
	#context = RequestContext(request, {'data':data, })

	#template = loader.get_template('genoapp/Homepage.html')
	#context = RequestContext(request, {'data':data, })

	#return HttpResponse(template.render(context))

def register(request):
#	person_list = Person.objects
#	template = loader.get_template('genoapp/Login.html')
#	context = RequestContext(request, {'person list':person_list, })
#	person_list = Person.objects
#	context = RequestContext(request, {'person list':person_list, })
#	template = loader.get_template('genoapp/Login.html')
	form = RegistrationForm()
	return render_to_response('genoapp/Registration.html',{'form': form}, RequestContext(request))



@login_required
def user_home(request):

	serData = serializeData()
	#serData.getPrimaryPerson()
	primary_person = request.user #serData.getPrimaryPerson()
	primary_account = Account.objects.filter(primaryUser=primary_person)
	parents = serData.getParents(primary_person)
	persons = Person.objects.filter(account=primary_account)

	form = PersonForm()
	context = RequestContext(request)
	template = loader.get_template('genoapp/user_home.html')
	#return render_to_response('genoapp/user_home.html',{"peopleData":serData.serializeToJson()})
	return render_to_response('genoapp/user_home.html',{"peopleData":"temporaryValue", 'form': form, 'persons': persons, 'current_user': primary_person}, RequestContext(request))

def user_registration(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/home/')

	if request.method == 'POST':
		form = RegistrationForm(request.POST)
		if form.is_valid():

			#pdb.set_trace()

			new_user = User.objects.create_user(
				username=form.cleaned_data['username'],
				first_name=form.cleaned_data['firstname'],
				last_name=form.cleaned_data['lastname'],
				email=form.cleaned_data['email'],
				password=form.cleaned_data['password']
			)
			new_user.save()
			#pdb.set_trace()
			account = Account(primaryUser=new_user)
			account.save()

			new_person = Person(
				user=new_user,
				firstName=form.cleaned_data['firstname'],
				lastName=form.cleaned_data['lastname'],
				gender=form.cleaned_data['gender'],
				account=account
			)
			new_person.save()
			#pdb.set_trace()
			health = HealthProfile(person=new_person)
			health.save()

			#message.success(request, 'Thank you for signing up') #amit
			return HttpResponseRedirect('/home/')
		else:
			return render_to_response('register.html', {'form': form, 'errors':form.errors}, context_instance=RequestContext(request))
	else:
		'''user is not submitting a form, show them a blank registration form'''
		form = RegistrationForm()
		context = {'form': form}
		return render_to_response('register.html',context, context_instance=RequestContext(request))


#	return HttpResponse(template.render(context))



def logout_request(request):
	logout(request)
	return HttpResponseRedirect('/')

def login(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/home/')

	if request.method == 'POST':
		#pdb.set_trace()
		username = request.POST.get('username') #['username'] #form.cleaned_data['username']
		password = request.POST.get('password') #['password'] #form.cleaned_data['password']
		user = authenticate(username=username, password=password)
		#pdb.set_trace()
		if user is not None:
			login_user(request, user)
			return HttpResponseRedirect('/home/')
		else:
			return render_to_response('genoapp/login.html', {}, context_instance=RequestContext(request))
	else:
		''' user is not submitting the form, show the login form '''
		#form = LoginForm()
		#context = {'form': form}
		return render_to_response('genoapp/login.html', {}, context_instance=RequestContext(request)) #render_to_response('login.html', context, context_instance=RequestContext(request))

def registration(request):
	return render_to_response('genoapp/registration.html', RequestContext(request))

def logout(request):
	msg = ""

	if request.user.is_authenticated():
		try:
			auth.logout(request)
			return render_to_response('genoapp/Homepage.html', RequestContext(request))
			msg = "You are logged out"
		except:
			msg = "Exception during the logout process"
		return render_to_response('genoapp/Homepage.html', RequestContext(request))
	else:
		return HttpResponseRedirect('/')

@login_required
def add_person(request):
	if request.method == 'POST':
		form = PersonForm(request.POST)

		if form.is_valid():
			account = Account.objects.get(primaryUser=request.user)
			current_person = Person.objects.get(user=request.user)
			gender = utilities.mapGender(form.cleaned_data['relationship_type'])

			new_person = Person(
				firstName=form.cleaned_data['firstname'],
				lastName=form.cleaned_data['lastname'],
				gender=gender,
				account=account
			)
			new_person.save()

			new_relationship = Relation(
				relationship_type=form.cleaned_data['relationship_type'],
				to_person=new_person,
				from_person=current_person
			)
			new_relationship.save()

			current_person.relationships.add(new_relationship)
			current_person.save()

			#pdb.set_trace()

			inverse_type = utilities.mapInverseRelationship(form.cleaned_data['relationship_type'], current_person.gender)
			#pdb.set_trace()
			inverse_relation = Relation(
				relationship_type=inverse_type,
				to_person = current_person,
				from_person = new_person
			)
			inverse_relation.save()
			new_person.relationships.add(inverse_relation)
			new_person.save()

			return HttpResponseRedirect('/home/')
			#persons =
			#return render_to_response('genoapp/user_home.html', RequestContext(request))
			#return HttpResponseRedirect('/home/')
		else:
			primary_person = request.user #serData.getPrimaryPerson()
			primary_account = Account.objects.filter(primaryUser=primary_person)
			persons = Person.objects.filter(account=primary_account)
			return render_to_response('genoapp/user_home.html', {'form': form, 'persons': persons}, RequestContext(request))
	else:
		return HttpResponseRedirect('/home/')


def user(request):
	pass

def playground(request):
	return render_to_response('genoapp/playground.html', RequestContext(request))

def tree(request):
	pass
