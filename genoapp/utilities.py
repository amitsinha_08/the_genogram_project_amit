from genoapp.models import RELATIONSHIP_TYPE

def mapInverseRelationship(input_type, gender):
    mapped = ''

    if gender == 'male':
        mapped = {
            'father': 'son',
            'mother': 'son',
            'brother':'brother',
            'sister':'brother',
            'daughter': 'father',
            'son' : 'father'
        }.get(input_type)
    else:
        mapped = {
            'father': 'daughter',
            'mother': 'daughter',
            'brother':'sister',
            'sister':'sister',
            'daughter': 'mother',
            'son' : 'mother'
        }.get(input_type)

    return mapped


def mapGender(relationship_type):
    gender = {
        'father': 'male',
        'mother': 'female',
        'brother': 'male',
        'sister': 'female',
        'son': 'male',
        'daughter': 'female'
    }.get(relationship_type)

    return gender
