from django.core.urlresolvers import resolve
from django.test import TestCase
from genoapp.models import Person, Relations
from genoapp import views
from django.http import HttpResponse, HttpRequest
from django.db import models
from genoapp import forms
from genoapp.forms import RegistrationForm


# Create your tests here.
class SmokeTest(TestCase):

	def test_home_page_returns_valid_route(self):
		found = resolve('/')
		self.assertEqual(found.func, views.index)

	def test_home_page_returns_valid_template(self):
		request = HttpRequest()
		response = views.index(request)
		self.assertIn(b'<title>Genogram Homepage</title>', response.content)

	def test_login_page_returns_a_valid_page(self):
		request = HttpRequest()
		response = views.register(request)
		self.assertEqual(2, response.status_code/100)

	def test_creating_a_new_person_object_and_saving_to_database(self):	
		#creating a new person object
		person = Person()
		#relation = Relations()
		#relation.relationship_type = 'Friends'
		#relation.related_to = person
		person.firstName = "Sudipta"
		person.lastName = "Mohapatra"
		person.gender = 0
		#person.relationship = relation
		
		# checking to see if we can save it
		#relation.save()
		person.save()

		#Checking in database
		all_persons_in_database = Person.objects.all()
		self.assertEquals(len(all_persons_in_database), 1)
		only_person_in_database = all_persons_in_database[0]
		self.assertEquals(only_person_in_database, person)

		#Checking to see if its saved the three attributes
		self.assertEquals(only_person_in_database.firstName, 'Sudipta')
		self.assertEquals(only_person_in_database.lastName, 'Mohapatra')
		self.assertEquals(only_person_in_database.gender, 0)
		
		

"""	def test_home_page_allows_form_submission():
		pass

	def test_user_models_can_be_created():
		pass

	def test_user_models_can_be_retrieved():
		pass

	def test_can_create_new_user():
		pass

	def test_can_create_person():
		pass

	def test_can_retrieve_person():
		pass

	def test_login_success_redirects_to_landing_page():
		pass

	def test_invalid_user_account_cannot_login():
		pass

	def test_can_list_created_persons():
		pass

"""




"""class PersonModelTest(TestCase):

	def setUp(self):
		Person.objects.create(firstname='Joe', lastname='Smith')
		Person.objects.create(firstname='Jane', lastname='Doe')

	def test_model_exists():
		joe = Person.objects.get(name='Joe')
		self.assertTrue(isinstance(joe, Person))

	def test_model_can_be_saved():
		tom = Person.objects.create(firstname='Tom', lastname='Jones')
		retrieved = Person.objects.get(firstname='Tom')
		self.assertEqual(retrieved.firstname, 'Tom')

	def test_model_can_be_deleted():
		tom = Person.objects.create(firstname='Tom', lastname='Jones')
		retrieved = Person.objects.get(firstname='Tom')
		retrieved.delete()

		try:
			obj = Person.objects.get(firstname='Tom', lastname='Jones')
		except:
			obj = None

		self.assertEqual(None, obj)

	def test_model_can_be_updated():
		pass

	def test_persons_have_correct_properties():
		pass

	def test_persons_can_have_multiple_connections():
		pass

	def test_persons_can_create_multiple_connections():
		pass
"""



