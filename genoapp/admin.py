from django.contrib import admin
from genoapp.models import Person, Relation, HealthProfile, Account

# Register your models here.
admin.site.register(Account)
admin.site.register(HealthProfile)


# class PersonAdmin(admin.ModelAdmin):
#     list_display = ['firstName', 'lastName', 'relationships']



class RelationsAdmin(admin.ModelAdmin):
    list_display = ['relationship_type', 'person']


admin.site.register(Relation, RelationsAdmin)
admin.site.register(Person) #PersonAdmin
