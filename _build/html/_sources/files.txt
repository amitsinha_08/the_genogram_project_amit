Documentation Of Files
======================

This page contains a brief overview of files critical to maintaining and keep the genogram website running. It provides all the essential information on how these files work and will be very usefull when making future updates to the project.

models.py
----
This file contains the definition of all the models for the database objects. Making any changes to the database, such as changing any database table, or the attributes of a table, or relationship between two tables is all handled here.

Here are the current models and their description below:

Person
^^^^
This table contains information on the people who are added or are a node of the genogram tree. Everytime a person signs up on the genogram website, a Person record is also created for him/her which will show up on his/her landing page showing then "You" node.

The Person table is has many to many relationship between Relation table and has a one to one relationship with the Account table. The gender field on the person table can have one of the 2 predefined values of the person being either 'male' or 'female'.


Relation
^^^^
This table is a junction between one Person to another Person. It also stores the relationship between the two Persons. This table enables us for a person to be related to one or many persons.


Account
^^^^
When a user registers on the website, a User object record is created for them. At the same time, an Account is also created for them. The primary use of this database model is to all the genoapp to distinguish between all the Person records that user has created and help them distinguish which Person is the Account owner.


HealthProfile
^^^^
This database model is being used to store the medical history of a person. It has a one to one relationship to the Person model. It stores the persons age, and physical/mental health related issues.



views.py
----
This file takes a web request and returns a web response. The view itself contains whatever logic is necessary to return that response.


def index(request):
^^^^
Takes in the request for the home page and returns a response by sending the user to 'genoapp/Homepage.html' page.


def user_home(request):
^^^^
This view takes the request of sending the users to their user home page once they have successfully logged in and as a response sends them to this page with the genogram view on it.


def user_registration(request):
^^^^
This view does multiple fuctions. It takes the request from the registration page, takes the registration field input parameters, and uses them to create a User record for the person in the database. It also creates a Person record for the newly registered user which is used to generate the very first genogram tree on the user home page showing the registered user as "You". It also creates an Account object record for this user, which will help distinguish this user as the owner of the genogram tree by distinguishing him/her as the account owner in the persons table compared to the other persons related to him/her.


def login(request):
^^^^
This view method takes the request to log in a user. It takes in the username and password input parameters entered in the log in form as a POST request and processes it. If the credentials seem to be valid, then it will login the user and as a response redirect them to their user home page. If, however, the credentials are invalid (bad user and/or password), then it will give an error message saying which of the fields (username and/or password) seems to be incorrect and require them to try again.


def logout(request):
^^^^
This method takes the request to logout a logged in user, end their current session, log them out and then as a response redirect them to a webpage which tells the user that he/she has been logged out.


def add_person(request):
^^^^
This view method is responsible for processing the add user form. It creates new Person record for each new person added in relationship with the current user or the genogram tree owner. It establishes the relationship between the two and also sets the new persons gender based on the relationship, create and inverse relationship and redirech them back to the user home page with the updated genograp.


forms.py
----
This is the file where django recommends to place all the form code. This helps keep the form code easy to access and maintain.


RegistrationForm
^^^^
This form contains all the required fields which the user needs to fill out to be able to register before being able to log in (first time users who do not have an account set up yet) and start building their genogram tree. The fields this form covers is the very basic ones needed to set up an account such as the username, email, birthday, firstname, lastname, password, verify password, and gender.


LoginForm
^^^^
This is the simplest form being used as it takes 2 input field values, username and password, as request to be processed for verification for user log in.


PersonForm
^^^^
This form contains fields required to add person related to the user person record and establish relationship between the two.

