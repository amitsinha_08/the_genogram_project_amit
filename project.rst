Project Summary
===============
We thought it would be a great idea to go over what made us build this website and what our goals were when we started development of the site.

Goals Intended (User Stories)
-----------------------------
As a user I want to be able to register for a new account using at least my name, email address, date of birth so I can start creating my genograms.
^^^^

As a user, I want to be able to view a tree of my family history.
^^^^


Goals Achieved (User Stories Completed)
---------------------------------------



Lessons Learned
---------------


