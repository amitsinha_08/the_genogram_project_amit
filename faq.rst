Frequently Asked Questions
==========================

Q. Can I create a genogram tree even if I do not have an account?
----
A. No. We need you to Sign up and then be logged in before you can start creating your genogram.

Q. How can I edit or remove a family member I have added to my Genogram tree?
----
A. Currently we do not have the feature which would allow users to edit or remove existing family members in their genogram tree. We plan to have it implemented in the near future.


Q. Can I view the genogram tree of a family member of mine who also uses this website?
----
A. This feature is not supported by the current version of genoapp. Again, given much more time, we would implement it.


