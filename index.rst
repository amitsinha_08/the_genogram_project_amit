.. Genogram_Doc documentation master file, created by
   sphinx-quickstart on Thu May  8 14:31:47 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Genogapp!
====================
Genoapp is a web application which helps you to build a genogram in a very user friendly and intutive manner. It will unlock a whole new perspective the way you view your family tree and provide a meaningfull way to look at it.

What is a Genogram?
-------------------
A genogram is a family tree diagram of a person's family relationships and medical history. Families are complex systems that interact with other communities and kin groups, of which they are a part of, and they cannot be fully understood without the wider context in which they live. Genograms contain a wealth of information on the families represented because they allow you to illustrate not only how members of a family tree relate to each other, but how they are a product of their time, by their behaviors, friendships, and many more. 

Why would anyone need genogram tree?
------------------------------------
A genogram allows you to depict significant persons and events in your family's history. Genograms can also include annotations about the medical history and major personality traits of each member of your family. Genograms will help you uncover intergenerational patterns of behavior, marriage choices, family alliances and conflicts, the existence of family secrets, and other information that will shed light on your family's present situation. What better reason to do genealogy than to be able to learn life lessons from your ancestors...

How do I get started?
---------------------
First time users are needed to sign up before they can get busy with building their genogram trees. If you already have a user account, then all you need to do is log in the website and you will be taken to your home page. First time users, or users who have not yet created a family tree will see their home screen just showing them there labeled "You". 

To start adding family members, use the "Add a family member" section on your home page, which will allow you to add new family members to your genogram tree. Here you get the option to fill out their names, their relationship to you, and any history of mental or physical ailments. The medical history is the heart of the genogram tree as it will help you understand if you may face any similar risks.

Currently unavailable is the feature to allow users to edit the people in their genographs and/or remove them.


Contents:

.. toctree::
   :maxdepth: 2

   faq
   readme
   project
   files

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

